package logic;

import java.util.ArrayList;

import gui.GamePanel;
import model.Spaceship;

public class Loop {

	private boolean gameRunning;
	private MainLogic ml;
	private GamePanel gp;
	private GameMouse gm;
	private Selection select;

	public Loop(GamePanel gp, MainLogic ml) {
		this.gameRunning = false;
		this.gp = gp;
		this.ml = ml;

	}

	public void gameLoop() {
		long lastLoopTime = System.nanoTime();
		final int TARGET_FPS = 60;
		System.out.println("Optimally FPS: 60");
		final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;
		long lastFpsTime = 0;
		int fps = 0;
		gm = gp.getGameMouse();
		select = gp.getSelection();
		gameRunning = true;

		while (gameRunning) {
			long now = System.nanoTime();
			long updateLength = now - lastLoopTime;
			lastLoopTime = now;
			double delta = updateLength / ((double) OPTIMAL_TIME);

			//Aktualisiere Aktualisierungszeit
			lastFpsTime += updateLength;
			fps++;

			//Nach einer Sekunde: Erstelle neue Raumschiffe, Setzte FPS auf 0 zur�ck
			if (lastFpsTime >= 1000000000) {
				lastFpsTime = 0;
				System.out.println("Reached FPS: " + fps);
				fps = 0;
				ml.spawn();
			}

			//Setzt die Selektion beim loslassen der Maustaste
			if (gm.getPos()[0][0] == -1 && gm.getPos()[0][1] == -1 && gm.getPos()[1][0] == -1
					&& gm.getPos()[1][1] == -1) {
				select.setSelection(ml.getSpaceships(), gm);
			}
			doGameUpdates(delta);
			gp.repaint();

			try {
				Thread.sleep((lastLoopTime - System.nanoTime() + OPTIMAL_TIME) / 1000000);
			} catch (Exception e) {

			}
		}
	}

	//Update Spaceships
	private void doGameUpdates(double delta) {
		ArrayList<Spaceship> sps = ml.getSpaceships();
		for (int i = 0; i < sps.size(); i++) {
			if (gm.isRightClick()) {
				if (sps.get(i).isSelected()) {
					sps.get(i).setxDestination(gm.getDestination()[0]);
					sps.get(i).setyDestination(gm.getDestination()[1]);
					sps.get(i).setSelected(false);
				}
			}
			sps.get(i).move(delta);
		}
		gm.setRightClick(false);

		for (int i = 0; i < sps.size(); i++) {

			for (int j = 0; j < sps.size(); j++) {
				if (i != j) {
					sps.get(i).distribute(sps.get(j), delta);
				}
			}
		}
	}

	public boolean isGameRunning() {
		return gameRunning;
	}

	public void setGameRunning(boolean gameRunning) {
		this.gameRunning = gameRunning;
	}

}
