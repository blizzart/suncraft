package logic;

import java.util.ArrayList;

import gui.MainFrame;
import model.SmallSpaceship;
import model.Spaceship;
import model.Sun;

public class MainLogic {

	private static MainLogic instance;
	private MainFrame mf;
	private ArrayList<Spaceship> sp;
	private ArrayList<Sun> suns;
	private Loop lp;

	private MainLogic() {
		suns =  new ArrayList<Sun>();
		sp = new ArrayList<Spaceship>();
		testInit();
	}
	
	//Gibt die vorhandene Instanz des Objekt zur�ck oder erstellt eine
	public static MainLogic getInstance() {
		if (MainLogic.instance == null) {
			MainLogic.instance = new MainLogic();
		}
		return MainLogic.instance;
	}
	
	//Initialisierung zum Testen
	private void testInit(){
		Sun s1 = new Sun(100, 100, 100, 100, 10, 1);
		suns.add(s1);
		SmallSpaceship sm = new SmallSpaceship(200, 200, 10, 10, 1, 1);
		sp.add(sm);
	}
	
	//�berpr�ft ob das MainFrame bereits gesetzt ist, wenn ja starte Schleife
	public void startLoop(){
		if(mf != null){
			lp = new Loop(mf.getGamePanel(), instance);
			lp.setGameRunning(true);
			lp.gameLoop();
		}
	}
		
	//Ruft f�r jedes Sun Objekt die Methode spawnSpaceship() auf
	public void spawn(){
		for (Sun sun : suns) {
			sp.add(sun.spawnSpaceship());
		}
	}
	
	//Stoppt die Schleife
	public void stopLoop(){
		lp.setGameRunning(false);
	}
	
	//Setzt das MainFrame
	public void setMainFrame(MainFrame mf) {
		this.mf = mf;
	}
	
	public ArrayList<Spaceship> getSpaceships() {
		return sp;
	}

	public ArrayList<Sun> getSuns() {
		return suns;
	}
	

}
