package logic;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.SwingUtilities;

public class GameMouse extends MouseAdapter implements MouseMotionListener {

	private int[][] pos = new int[2][2];
	private int[] destination = new int[] { 0, 0 };
	private boolean isRightClick = false;

	//Wenn "Rechts-Klick" setzte Destination, wenn nicht, setzte Position
	public void mousePressed(MouseEvent me) {
		if (SwingUtilities.isRightMouseButton(me)) {
			this.destination = new int[] { me.getPoint().x, me.getPoint().y };
			this.isRightClick = true;
		} else {
			pos = new int[][] { { me.getPoint().x, me.getPoint().y }, { me.getPoint().x, me.getPoint().y } };
		}
	}

	//Setzt die Position beim ziehen der Maustaste
	public void mouseDragged(MouseEvent me) {
		if (SwingUtilities.isLeftMouseButton(me)) {
			pos[1][0] = me.getPoint().x;
			pos[1][1] = me.getPoint().y;
		}
	}

	//Setzt die Position beim loslassen der Maustaste
	public void mouseReleased(MouseEvent me) {
		if (!SwingUtilities.isRightMouseButton(me)) {
			pos = new int[][] { { -1, -1 }, { -1, -1 } };
		}
	}

	public int[][] getPos() {
		return pos;
	}

	public void setPos(int[][] pos) {
		this.pos = pos;
	}

	public int[] getDestination() {
		return destination;
	}

	public void setDestination(int[] destination) {
		this.destination = destination;
	}

	public boolean isRightClick() {
		return isRightClick;
	}

	public void setRightClick(boolean isRightClick) {
		this.isRightClick = isRightClick;
	}

}
