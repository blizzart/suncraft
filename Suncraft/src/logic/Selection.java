package logic;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import model.Spaceship;

public class Selection {

	private int[][] pos;

	//Malt das Auswahl Rechteck
	public void select(Graphics g, GameMouse gm) {
		g.setColor(Color.white);
		int[] x = new int[] { gm.getPos()[0][0], gm.getPos()[1][0] };
		int[] y = new int[] { gm.getPos()[0][1], gm.getPos()[1][1] };

		g.drawPolygon(new int[] { x[0], x[0], x[1], x[1] }, new int[] { y[0], y[1], y[1], y[0] }, 4);
		if (!check(gm)) {
			this.pos = gm.getPos();
		}
	}

	//�berpr�ft ob die Maustaste losgelassen wurde
	public boolean check(GameMouse gm) {
		if (gm.getPos()[0][0] == -1 && gm.getPos()[0][1] == -1 && gm.getPos()[1][0] == -1 && gm.getPos()[1][1] == -1) {
			return true;
		} else {
			return false;
		}
	}
	
	//Setzt alle im Auswahlrechteck befindlichen Raumschiffe auf seclected = true
	public void setSelection(ArrayList<Spaceship> sps, GameMouse gm) {
		sortSelection();
		if (check(gm)) {
			for (int i = 0; i < sps.size(); i++) {
				if ((this.pos[0][0] <= sps.get(i).getX() && sps.get(i).getX() <= this.pos[1][0])
						&& (this.pos[0][1] <= sps.get(i).getY() && sps.get(i).getY() <= this.pos[1][1])) {
					sps.get(i).setSelected(true);
				}
			}
		}
		gm.setPos(new int[][] { { 0, 0 }, { 0, 0 } });
	}

	//Sortiert die Koordinaten des Auswahlrechtecks
	private void sortSelection() {
		if (this.pos[0][0] < this.pos[1][0] && this.pos[0][1] > this.pos[1][1]) {
			this.pos = new int[][] { { this.pos[0][0], this.pos[1][1] }, { this.pos[1][0], this.pos[0][1] } };

		} else if (this.pos[0][0] > this.pos[1][0] && this.pos[0][1] < this.pos[1][1]) {
			this.pos = new int[][] { { this.pos[1][0], this.pos[0][1] }, { this.pos[0][0], this.pos[1][1] } };

		} else if (this.pos[0][0] > this.pos[1][0] && this.pos[0][1] > this.pos[1][1]) {
			this.pos = new int[][] { { this.pos[1][0], this.pos[1][1] }, { this.pos[0][0], this.pos[0][1] } };

		}
	}

}
