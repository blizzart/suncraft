package gui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import logic.GameMouse;
import logic.Selection;

public class GamePanel extends JPanel {

	private GameMouse gm = new GameMouse();
	private Selection select = new Selection();
	private BufferedImage bf;

	public GamePanel() {
		super();
		addMouseListener(gm);
		addMouseMotionListener(gm);
		try {
            bf = ImageIO.read(new File("./images/space.jpg"));
        } catch(IOException e) {
            e.printStackTrace();
        }
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(bf, 0,0 ,this.getWidth(), this.getHeight(), this);
		SunDrawer.drawSun(g);
		SpaceshipDrawer.drawSpaceships(g);
		select.select(g, gm);
		
	}
	
	public GameMouse getGameMouse(){
		return this.gm;
	}
	
	public Selection getSelection(){
		return this.select;
	}
}
