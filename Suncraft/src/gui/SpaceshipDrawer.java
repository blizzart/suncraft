package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import logic.MainLogic;
import model.Spaceship;

public abstract class SpaceshipDrawer {

	private static MainLogic ml;

	// Zeichnet alle vorhandenen Spaceships auf das GamePanel
	public static void drawSpaceships(Graphics g) {
		ml = MainLogic.getInstance();
		ArrayList<Spaceship> sp = ml.getSpaceships();
		for (Spaceship spaceship : sp) {
			g.setColor(Color.white);
			g.fillOval((int) (spaceship.getX()) - 5, (int) (spaceship.getY()) - 5, spaceship.getWidth(),
					spaceship.getHeight());
			g.setColor(Color.black);
			g.drawOval((int) (spaceship.getX()) - 5, (int) (spaceship.getY()) - 5, spaceship.getWidth(),
					spaceship.getHeight());
		}

	}

}
