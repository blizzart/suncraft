package gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import logic.MainLogic;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	private MainFrame instance = this;
	private MainLogic ml;
	private GamePanel gp;

	public void createMainFrame() {
		ml = MainLogic.getInstance();
		ml.setMainFrame(instance);
		ml.startLoop();

	}

	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setSize(600, 450);
		setTitle("Suncraft by BlizzArt");
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		gp = new GamePanel();
		gp.setSize(300, 300);
		gp.setBackground(java.awt.Color.lightGray);
		contentPane.add(gp, BorderLayout.CENTER);

		setVisible(true);

	}

	public GamePanel getGamePanel() {
		return gp;
	}

}
