package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import logic.MainLogic;
import model.Sun;

public abstract class SunDrawer {

	private static MainLogic ml;
	

	// Zeichnet alle vorhandenen Sonnen auf das GamePanel
	public static void drawSun(Graphics g) {
		ml = MainLogic.getInstance();
		BufferedImage bf = null;
		ArrayList<Sun> suns = ml.getSuns();
		for (Sun sun : suns) {
			g.setColor(Color.yellow);
			try {
	             bf = ImageIO.read(new File("./images/sonne.png"));
	        } catch(IOException e) {
	            e.printStackTrace();
	        }
			//g.fillOval((int) (sun.getX()), (int) (sun.getY()), sun.getWidth(), sun.getHeight());
			g.drawImage(bf, (int) (sun.getX()),(int) (sun.getY()) ,sun.getWidth(), sun.getHeight(), null);
		}
	}
}