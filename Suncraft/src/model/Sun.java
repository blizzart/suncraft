package model;

public class Sun extends Unit {

	public Sun() {

	}

	public Sun(double x, double y, int width, int height, int leben, int minLeben) {
		super(x, y, width, height, leben, minLeben);
	}

	//Spawnt Spaceship an zuf�lliger Position in der N�he der Sonne
	public Spaceship spawnSpaceship() {
		Spaceship milleniumFalke = new Spaceship(this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2, 10, 10, 1, 1);
		return milleniumFalke;
	}
}
