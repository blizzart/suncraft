package model;

public abstract class Unit {
	
	private double x;
	private double y;
	private int width;
	private int height;
	private int leben;
	private int minLeben;
	private int radius;
	
	public Unit(){

	}

	public Unit(double x, double y, int width, int height, int leben, int minLeben) {

		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.leben = leben;
		this.minLeben = minLeben;
		this.radius = this.width / 2;

	}
	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getLeben() {
		return leben;
	}
	public void setLeben(int leben) {
		this.leben = leben;
	}
	public int getMinLeben() {
		return minLeben;
	}
	public void setMinLeben(int minLeben) {
		this.minLeben = minLeben;
	}
}
