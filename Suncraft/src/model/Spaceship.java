package model;

import java.util.Random;

public class Spaceship extends Unit {

	private final int SPEED = 1;
	private double xDestination = this.getX();
	private double yDestination = this.getX();
	private boolean isSelected = false;

	public Spaceship(double x, double y, int width, int height, int leben, int minLeben) {
		super(x, y, width, height, leben, minLeben);
	}

	public String toString() {
		return "(" + this.getX() + ", " + this.getY() + "," + this.getWidth() + "," + this.getHeight() + ", "
				+ this.getLeben() + "," + this.getMinLeben() + ")";
	}

	public void distribute(Spaceship sp, double delta) {
		double xdiv = sp.getX() - this.getX();
		double ydiv = sp.getY() - this.getY();

		double distance = Math.sqrt(xdiv * xdiv + ydiv * ydiv);

		if (distance <= this.getRadius() + sp.getRadius()) {
			if (distance == 0) {
				distance = 1;
				xdiv = 1;
				ydiv = 1;
			}
			Random rand = new Random();
			this.setX(this.getX() + ((-1 * xdiv + (rand.nextInt(3) - 1)) / distance * this.SPEED * delta));
			this.setY(this.getY() + ((-1 * ydiv) / distance * this.SPEED * delta));
		}
	}

	public void move(double delta) {
		double xdiv = this.xDestination - this.getX();
		double ydiv = this.yDestination - this.getY();

		double distance = Math.sqrt(xdiv * xdiv + ydiv * ydiv);
		if ((distance > this.getRadius() + 1)) {
			this.setX(this.getX() + (xdiv / distance * this.SPEED * delta));
			this.setY(this.getY() + (ydiv / distance * this.SPEED * delta));
		} else {
			this.xDestination = this.getX();
			this.yDestination = this.getY();
		}

	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public double getxDestination() {
		return xDestination;
	}

	public void setxDestination(double xDestination) {
		this.xDestination = xDestination;
	}

	public double getyDestination() {
		return yDestination;
	}

	public void setyDestination(double yDestination) {
		this.yDestination = yDestination;
	}
}
